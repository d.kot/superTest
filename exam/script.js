var slideIndex = 1;
showSlides(slideIndex);

function plusSlide() {
    showSlides(slideIndex += 1);
}

function minusSlide() {
    showSlides(slideIndex -= 1);  
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("item");
    var dots = document.getElementsByClassName("slider-dots_item");
    if (n > slides.length) {
      slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
}


//sddfsdf

window.addEventListener("DOMContentLoaded", function() {
    var left = document.querySelector(".slider__left"),
        right = document.querySelector(".slider__right"),
        container = document.querySelector(".slider__container"),
        timer;
    right.addEventListener("click", function() {
        window.clearTimeout(timer);
        var last = container.querySelector(".slider__item:last-child");
        var first = container.querySelector(".slider__item:first-child");
        container.classList.remove("animate");
        if (!container.classList.contains("left")) {
            container.classList.add("left");
            container.insertBefore(last, first);
        }
        timer = window.setTimeout(function() {
            container.classList.add("animate");
            container.classList.remove("left")
        },30)
    });
    left.addEventListener("click", function() {
        window.clearTimeout(timer);
        var first = container.querySelector(".slider__item:first-child");
        container.classList.remove("animate");
        if (container.classList.contains("left")) {
            container.classList.remove("left");
            container.appendChild(first)
        }
        timer = window.setTimeout(function() {
            container.classList.add("animate");
            container.classList.add("left")
        },30)
    })
});