/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    Company | Balance | Показать дату регистрации | Показать адресс |
    1. CompName 2000$ button button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/

async function getCompanyList() {
	const ListOfCompany = await fetch ('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2');
	const Company = await ListOfCompany.json();
	console.log(Company);
    let div = document.querySelector('#async');
    console.log(div)
    let table = document.createElement('table');
    table.border = '1px solid black';
    table.style.borderCollapse = 'collapse'
    table.innerHTML = `<tr><td>Company</td><td>Balance</td><td>Показать дату регистрации</td><td>Показать адресс</td></tr>`
    div.appendChild(table)
    let showDate
    Company.map(item  => {
        let a = ``
        let tr = document.createElement('tr');
        tr.innerHTML = `
                        <td>${item.company}</td>
                        <td>${item.balance}</td>
                        <td class='date'><button class='_showDate'>Show Date</button></td>
                        <td class='adress'><button class='_showAdress'>Show Adress</button></td>
                        `
        table.appendChild(tr);        

       
    })    

let showD = div.querySelectorAll('._showDate');
        let showArr = Array.from(showD);
        showArr.forEach(button =>  {
            button.addEventListener('click', (e)=>{
              



              //   console.log(showArr.indexOf(e.target))
              // let datestr = document.querySelectorAll('.date');
              // let datestrArr = Array.from(datestr);
              // let d = Company.map(i=>{i[showArr.indexOf(e.target)]})
              // datestrArr[showArr.indexOf(e.target)].appendChild(d)
              // console.log(datestrArr) 
            })  
        })
let showA = div.querySelectorAll('._showAdress');
        let showAd = Array.from(showA);
        showAd.forEach(button =>  {
            button.addEventListener('click', (e)=>{
                console.log(e.target)
            })  
        })
        
    
}

getCompanyList();