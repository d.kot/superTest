 /*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/

	var train = {
		name: "Nadezhda",
		speed: "228 km/h",
		quantity: 327,


		roll: function(){
			console.log('Поезд '+ this.name +' везет '+ this.quantity + ' со скоростью ' + this.speed)
		},

		stop: function(){
			this.speed = "0 km/h"
			console.log('Поезд '+ this.name + ' остановился . Скорость '+this.speed )
		},
		pickUp: function(x){
			let newQuantity=this.quantity + x;
			console.log('Поезд подобрал '+ x +' пассажиров. Количество: ' + newQuantity);
		}
		
	}
	console.log(train);
	train.roll();
	train.stop();
	train.pickUp(123);